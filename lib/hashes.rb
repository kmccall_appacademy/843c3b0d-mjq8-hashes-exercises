# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words_and_lengths = Hash.new(0)
  str.split.each do |word|
    words_and_lengths[word] = word.length
  end
  words_and_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k ,v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each_key do |new_key|
    older[new_key] = newer[new_key]
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)
  word.each_char {|ch| letters[ch] +=1 }
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  no_dup = Hash.new
  arr.each { |el| no_dup[el] = 0}
  no_dup.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parities = {:even=>0, :odd=>0}
  numbers.each do |num|
    if num.even?
      parities[:even] +=1
    else
      parities[:odd] +=1
    end
  end
  parities
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  string_vowels = string.chars.sort.select { |ch| vowels.include?(ch)}
  vowel_freq = Hash.new(0)
  string_vowels.each { |ch| vowel_freq[ch] +=1 }
  vowel_freq.sort_by { |k, v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  bday_names = []
  result = []
  fwinter_bdays = students.each.select { |k, v| v > 6 }
  fwinter_bdays.each { |k, v| bday_names << k}
  (0...bday_names.length).each do |idx1|
    (idx1+1...bday_names.length).each do |idx2|
      result << [bday_names[idx1],bday_names[idx2]]
    end
  end
  result

end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  animals = Hash.new(0)
  specimens.each { |creature| animals[creature] +=1}
  num_species = animals.keys.length
  smallest_population_size = animals.sort_by { |k, v| v }[0][1]
  largest_population_size = animals.sort_by { |k, v| v }[-1][1]
  biodiversity_index = num_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_hash = Hash.new(0)
  normal_sign.downcase.each_char { |ch| normal_sign_hash[ch] += 1}
  vandalized_sign_hash = Hash.new(0)
  vandalized_sign.downcase.each_char { |ch| vandalized_sign_hash[ch] +=1}

  if vandalized_sign_hash.each.all? { |k, v| normal_sign_hash.include?(k)}
    vandalized_sign_hash.each.all? { |k, v| normal_sign_hash[k] >= v}
  else
    false
  end
end

def character_count(str)

end
